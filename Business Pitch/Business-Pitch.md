# Nomor 1
Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
- Use case user

| NO |  Use Case |
| ------ | ----- |
| 1 | Menambahkan akun baru|
| 2 | Membaca setiap informasi aplikasi yang ada |
| 3 | Mengupdate informasi pribadi/profile |
| 4 | Menghapus akun mereka jika tidak menggunakan Traveloka lagi  |
| 5 | User Bisa Pesan Tiket Pesawat   |
| 6 | User Bisa Pesan Hotel      |
| 7 | User Bisa Pesan Tiket Kereta Api |
| 8 | User Bisa Pesan Tiket Bus |
| 9 | User Bisa Mendapatkan Promo/diskon Terhadap pemesanan tiket |
| 10 | User bisa memberikan ulasan |
| 11 | User bisa memberikan rating |
| 12 | User bisa membatalkan pesanan |
| 13 | User melakukan pencarian tiket pesawat, kereta api, dan hotel |
| 14 | User melakukan pembayaran atas pemesanan tiket yang dipilih |
| 15 | User dapat melihat ulasan informasi t destinasi wisata tertentu |
| 16 | User dapat melihat riwayat tiket yang dipesan |
| 17 | User dapat melakukan pengaturan informasi akun |
| 18 | User dapat menghubungi layanan pelanggan untuk mendapatkan bantuan atau informasi lebih lanjut |
| 19 | User dapat mengganti jadwal pemesanan tiket tertentu |
| 20 | User dapat mengakses informasi tentang fasilitas dan layanan tambahan yang tersedia di hotel atau maskapai tertentu, seperti makanan khusus atau akses ke lounge VIP |
| 21 | User dapat mengunduh atau mencetak tiket atau dokumen penting terkait perjalanan atau penginapan yang sudah dipesan |
| 22 | User dapat melakukan perbandingan antara harga atau fasilitas yang ditawarkan oleh beberapa maskapai atau hotel berbeda, sehingga dapat memilih opsi yang paling sesuai dengan kebutuhan dan anggaran |
| 23 | User dapat Mengikuti program loyalty atau member untuk mendapatkan potongan harga atau keuntungan lain ketika melakukan pemesanan di Traveloka |
| 24 | User dapat memilih tempat duduk atau kamar hotel yang diinginkan, jika tersedia opsi untuk memilih sendiri |
| 25 | User dapat memilih tujuan destinati |
| 26 | User dapat memilih tipe kamar |
| 27 | User dapat melihat rentang harga tiket dari termurah dan termahal |
| 28 | User dapat mencari tiket konser |
| 29 | User dapat memlilih kategori tiket |
| 30 | User dapat melaukan pemesanan tiket konser |

- Use case manajemen perusahaan

| NO | Use Case |
|----|----------|
| 1  | Membuat dan mengelola akun pengguna admin |
| 2  | Membaca informasi pengguna, termasuk data pribadi dan riwayat pemesanan |
| 3  | Mengupdate informasi pengguna, seperti alamat email, nomor telepon, atau preferensi |
| 4  | Menghapus akun pengguna jika diperlukan |
| 5  | Menambahkan dan mengelola informasi tentang maskapai penerbangan, hotel, atau penyedia aktivitas wisata |
| 6  | Membaca informasi tentang maskapai penerbangan, hotel, atau aktivitas wisata, termasuk harga, ketersediaan, dan ulasan pengguna |
| 7  | Mengupdate informasi tentang maskapai penerbangan, hotel, atau aktivitas wisata, seperti harga, fasilitas, atau deskripsi |
| 8  | Menghapus informasi yang sudah tidak relevan atau tidak digunakan lagi, misalnya jika maskapai penerbangan atau hotel tidak bekerja sama lagi |
| 9  | Membuat dan mengelola promosi atau diskon untuk pemesanan tiket |
| 10 | Menganalisis data penjualan, pendapatan, dan pengguna untuk mendapatkan wawasan bisnis |
| 11 | Melihat laporan dan statistik mengenai kinerja bisnis secara keseluruhan |
| 12 | Mengelola hubungan dengan mitra bisnis, termasuk maskapai penerbangan, hotel, atau penyedia aktivitas wisata |
| 13 | Melakukan integrasi dengan sistem atau platform lain yang terkait |
| 14 | Mengelola inventori hotel, termasuk ketersediaan kamar, harga, dan promosi |
| 15 | Mengelola dan menganalisis data pelanggan, termasuk riwayat pemesanan dan preferensi pengguna |
| 16 | Mengawasi dan memantau operasional perusahaan secara real-time |
| 17 | Melihat dan menganalisis data tentang kepuasan pelanggan dan masalah yang terkait |
| 18 | Mengakses informasi tentang performa produk dan layanan, serta melihat tren pasar |
| 19 | Menggunakan alat analisis untuk membuat laporan kinerja dan proyeksi bisnis |
| 20 | Mengatur dan melacak kegiatan pemasaran, termasuk kampanye promosi dan iklan |

- Use case direksi perusahaan (dashboard, monitoring, analisis)

| NO | Use Case |
|----|----------|
| 1  | Melihat dashboard yang memberikan ringkasan kinerja perusahaan secara keseluruhan, termasuk pendapatan, laba, dan metrik kunci lainnya. |
| 2  | Memantau operasional perusahaan secara real-time, termasuk jumlah pemesanan, ketersediaan stok, dan status pengiriman. |
| 3  | Menerima peringatan atau notifikasi jika ada situasi darurat atau ketidaknormalan yang membutuhkan perhatian segera. |
| 4  | Melihat grafik dan diagram visual untuk memvisualisasikan data bisnis secara efektif. |
| 5  | Menyediakan akses cepat ke informasi penting, laporan, dan dokumen terkait. |
| 6  | Melacak kinerja tim atau departemen tertentu dalam menjalankan tugas mereka. |
| 7  | Memantau dan menganalisis tingkat kepuasan pelanggan berdasarkan umpan balik dan ulasan mereka. |
| 8  | Menganalisis data penjualan, pendapatan, dan keuntungan untuk mengidentifikasi tren dan peluang bisnis. |
| 9  | Melakukan segmentasi pelanggan berdasarkan preferensi, perilaku, dan demografi untuk menginformasikan strategi pemasaran dan penargetan. |
| 10 | Menyediakan alat prediksi dan proyeksi untuk membantu dalam perencanaan bisnis dan pengambilan keputusan jangka panjang. |
| 11 | Mengidentifikasi masalah operasional atau bottlenecks yang mempengaruhi efisiensi dan kualitas layanan. |
| 12 | Menggunakan analisis data untuk mengoptimalkan harga, promosi, dan strategi penjualan. |
| 13 | Melihat laporan dan statistik mengenai kinerja bisnis secara keseluruhan. |
| 14 | Menganalisis data pelanggan untuk mengidentifikasi peluang upselling, cross-selling, dan retensi. |
| 15 | Memantau dan mengelola risiko perusahaan, termasuk risiko keuangan, operasional, dan kepatuhan. |
| 16 | Melakukan pemantauan pasar dan analisis pesaing untuk menginformasikan strategi bisnis. |
| 17 | Mengatur dan melacak kegiatan pemasaran, termasuk kampanye promosi dan iklan. |
| 18 | Menggunakan alat analisis untuk membuat laporan kinerja dan proyeksi bisnis. |
| 19 | Mengelola hubungan dengan mitra bisnis, termasuk maskapai penerbangan, hotel, atau penyedia aktivitas wisata. |
| 20 | Melakukan integrasi dengan sistem atau platform lain yang terkait. |


---

# Nomor 2
Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
---

![Class Diagram](Assets/Class_Diagram.png)

# Nomor 3
Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
---
**- Single Resposibility Principle (SRP)**

Pada kelas ``Hotel``, ``HotelDAO`` dan ``HotelForm`` diterapkan prinsip SOLID SRP (Single Resposibility Principle) dengan analisis :

1. Kelas Hotel:

- Kelas ini bertanggung jawab untuk merepresentasikan informasi dan perilaku terkait hotel.
- Kelas ini memiliki beberapa atribut yang mewakili detail hotel seperti ID hotel, nama hotel, kota, fasilitas, harga, dan rating.
- Kelas ini memenuhi prinsip SRP karena hanya memiliki satu tanggung jawab yang berkaitan dengan representasi dan pengelolaan data hotel.

2. Kelas HotelDAO:

- Kelas ini bertanggung jawab untuk mengelola akses ke data hotel.
- Kelas ini berisi metode untuk melakukan operasi CRUD (Create, Read, Update, Delete) terhadap entitas hotel dalam database.
- Kelas ini memenuhi prinsip SRP karena tanggung jawabnya berfokus pada pengelolaan akses data terkait hotel.

3. Kelas HotelForm:

- Kelas ini bertanggung jawab untuk antarmuka pengguna terkait hotel.
- Kelas ini mengatur tampilan dan interaksi pengguna terkait operasi hotel, seperti menampilkan daftar hotel, menghitung total harga, dan melakukan pemesanan tiket.
- Kelas ini mematuhi prinsip SRP karena tanggung jawabnya terkait dengan antarmuka pengguna hotel.


- Kelas Hotel

```java
package pemesanan;

/**
 *
 * @author LENOVO
 */
public class Hotel {
    private int idHotel;
    private String namaHotel;
    private String kota;
    private String fasilitas;
    private int harga;
    private int rating;
    
    public Hotel(int idHotel, String namaHotel, String kota, String fasilitas, int harga, int rating){
        this.idHotel = idHotel;
        this.namaHotel = namaHotel;
        this.kota = kota;
        this.fasilitas = fasilitas;
        this.harga = harga;
        this.rating = rating;
    }
    
    // Setter
    public void setIdHotel(int idHotel) {
    this.idHotel = idHotel;
    }

    public void setNamaHotel(String namaHotel) {
        this.namaHotel = namaHotel;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setFasilitas(String fasilitas) {
        this.fasilitas = fasilitas;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
 
    // Getter
    public int getIdHotel(){
        return idHotel;
    }
    
    public String getNamaHotel(){
        return namaHotel;
    }
    
    public String getKota(){
        return kota;
    }
    
    public String getFasilitas(){
        return fasilitas;
    }
    
    public int getHarga(){
        return harga;
    }
    
    public int getRating(){
        return rating;
    }
}
```

- Kelas HotelDAO

```java
package pemesanan;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HotelDAO {
    private Connection connection;

    public HotelDAO() {
        connection = DatabaseConnection.getInstance().getConnection();
    }

    public List<Hotel> getAllHotel() {
        List<Hotel> tiketList = new ArrayList<>();

        try {
            String query = "SELECT * FROM hotel";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            
            while (resultSet.next()) {
                int idTiket = resultSet.getInt("id_hotel");
                String namaHotel = resultSet.getString("nama_hotel");
                String kota = resultSet.getString("kota");
                String fasilitas = resultSet.getString("fasilitas");
                int harga = resultSet.getInt("harga");
                int rating = resultSet.getInt("rating");
                
                Hotel hotel = new Hotel(idTiket, namaHotel, kota, fasilitas, harga, rating);
                tiketList.add(hotel);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tiketList;
    }
}
```

- Kelas HotelForm

    [SourceCode](TravelokaApp/src/pemesanan/HotelForm.java)

**- Open - Closed Principle**

Prinsip SOLID OCP (Open-Closed Principle) adalah prinsip desain perangkat lunak yang menyatakan bahwa sebuah entitas perangkat lunak (misalnya kelas atau modul) harus terbuka untuk perluasan (open for extension) namun tertutup untuk modifikasi (closed for modification). Artinya, ketika ada perubahan atau penambahan fitur baru, tidak perlu mengubah kode yang sudah ada, melainkan hanya perlu menambahkan kode baru atau mengganti bagian yang relevan.

Dalam kasus ini, saya memiliki kelas abstrak "Tiket" yang berfungsi sebagai entitas umum untuk tiket. Kemudian menerapkan kelas turunan "TiketPesawat" dan "TiketKeretaApi" yang memperluas fungsionalitas dari kelas abstrak "Tiket". Setiap kelas turunan memiliki implementasi yang berbeda untuk metode getTipeTiket(), yang merupakan contoh perluasan fungsionalitas tanpa mengubah kode di kelas abstrak. 

Berikut SourceCode yang menunjukkan adanya prinsip SOLID Open Closed Principle (OCP) :

- Kelas Abstrak Tiket

    [SourceCode](TravelokaApp/src/pemesanan/Tiket.java)

- Kelas Tiket Pesawat

    [SourceCode](TravelokaApp/src/pemesanan/TiketPesawat.java)

- Kelas Tiket Kereta Api

    [SourceCode](TravelokaApp/src/pemesanan/TiketKeretaApi.java)

# Nomor 4
Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
---

Design Pattern yang saya pilih adalah salah satunya Singleton Pattern

```java
public class DatabaseConnection {
    private static DatabaseConnection instance;
    private static final String URL = "jdbc:mysql://localhost:3306/traveloka_database";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "manchester";

    private Connection connection;

    private DatabaseConnection() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Koneksi ke database gagal: " + e.getMessage());
        }
    }

    public static DatabaseConnection getInstance() {
        if (instance == null) {
            synchronized (DatabaseConnection.class) {
                if (instance == null) {
                    instance = new DatabaseConnection();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
}
```
Variabel ``instance`` dideklarasikan sebagai private static agar dapat diakses secara global dalam kelas.

Konstruktor ``DatabaseConnection`` adalah private, sehingga tidak dapat diakses dari luar kelas. Ini memastikan bahwa objek DatabaseConnection hanya dapat dibuat melalui metode getInstance().

Metode ``getInstance()`` adalah metode publik dan statis yang memastikan hanya ada satu instansi DatabaseConnection yang dibuat.

Metode ``getConnection()`` digunakan untuk mendapatkan objek Connection dari instansi DatabaseConnection.

Dengan menggunakan ``Singleton Pattern`` ini,  dapat memperoleh instansi ``DatabaseConnection`` melalui metode ``getInstance()`` dan mendapatkan koneksi database melalui metode ``getConnection()``.

# Nomor 5
Mampu menunjukkan dan menjelaskan konektivitas ke database
---

Didalam program java yang saya buat, saya mencoba menghubungkannya dengan database MySQL

Hal Pertama adalah menambahkan dependecies ``mysql-connector-j``

- Import Library JDBC

```java
import java.sql.*;
```

Kode ini mengimpor kelas-kelas yang diperlukan dari paket java.sql untuk menghubungkan dan berinteraksi dengan database.

```java

public class DatabaseConnection {
    private static DatabaseConnection instance;
    private static final String URL = "jdbc:mysql://localhost:3306/traveloka_database";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "manchester7";

    private Connection connection;

    private DatabaseConnection() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Koneksi ke database gagal: " + e.getMessage());
        }
    }

    public static DatabaseConnection getInstance() {
        if (instance == null) {
            synchronized (DatabaseConnection.class) {
                if (instance == null) {
                    instance = new DatabaseConnection();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
}
```

Kelas DatabaseConnection adalah kelas utilitas yang bertanggung jawab untuk mengatur koneksi ke database MySQL menggunakan JDBC (Java Database Connectivity). Kelas ini mengikuti pola desain Singleton, yang berarti hanya ada satu instance dari kelas ini yang dapat digunakan dalam aplikasi.

Berikut adalah penjelasan mengenai komponen utama dari kelas DatabaseConnection:

1. Variabel dan Konstanta:

    - URL: Variabel konstan yang menyimpan URL koneksi ke database MySQL. Dalam contoh ini, koneksi menggunakan URL jdbc:mysql://localhost:3306/traveloka_database, yang menunjukkan koneksi ke database traveloka_database pada server lokal MySQL.
    - USERNAME dan PASSWORD: Variabel konstan yang menyimpan informasi username dan password untuk mengakses database.

2. Instance dan Constructor:

    - instance: Variabel yang menyimpan instance tunggal dari kelas DatabaseConnection. Implementasi menggunakan pola Singleton untuk memastikan bahwa hanya ada satu instance kelas yang dapat digunakan.

    - DatabaseConnection(): Konstruktor private yang dipanggil hanya sekali untuk membuat koneksi ke database menggunakan DriverManager.getConnection(). Jika koneksi gagal, pesan kesalahan akan ditampilkan.

3. Metode getInstance():

    - Metode ini digunakan untuk mendapatkan instance tunggal dari kelas DatabaseConnection. Jika instance belum ada, metode ini akan membuat instance baru menggunakan pola Singleton. Jika instance sudah ada, instance yang ada akan dikembalikan.
    - Implementasi menggunakan synchronized untuk menghindari pembuatan multiple instance dalam lingkungan konkurensi.

4. Metode getConnection():

    - Metode ini digunakan untuk mendapatkan objek Connection yang mewakili koneksi ke database.
    - Metode ini akan mengembalikan objek Connection yang telah dibuat oleh konstruktor pada saat instance pertama kali dibuat.

Dengan menggunakan kelas DatabaseConnection, Anda dapat memperoleh koneksi ke database MySQL dengan memanggil DatabaseConnection.getInstance().getConnection().

# Nomor 6
Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya 
---

Web Service ini dibuat menggunakan framework spark java. Operasi CRUD yang dilakukan meliputi data user yang tersambung ke MySQL . Berikut merupakan SourceCode yang sudah memiliki operasi CRUD :

- Installasi dependecies yang dibutuhkan sepert ``spark core``, ``gson``,``jetty server``, ``servlet api`` dll.

![LIBRARY](Assets/LIBRARY.png)

- Kelas UserService
```java
package Akun;

import pemesanan.DatabaseConnection;
import static spark.Spark.*;
import com.google.gson.Gson;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserService {

    private static Gson gson = new Gson();
    private static Connection connection;

    public static void main(String[] args) {
        // Mendapatkan objek koneksi dari DatabaseConnection
        connection = DatabaseConnection.getInstance().getConnection();

        // Mendapatkan semua pengguna
        get("/users", (req, res) -> {
            List<User> users = getAllUsers();
            return gson.toJson(users);
        });

        // Mendapatkan pengguna berdasarkan ID
        get("/users/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            User user = getUserById(id);
            if (user != null) {
                return gson.toJson(user);
            } else {
                res.status(404);
                return "User not found";
            }
        });

        // Membuat pengguna baru
        post("/users", (req, res) -> {
            User newUser = gson.fromJson(req.body(), User.class);
            int id = createUser(newUser);
            newUser.setId(id);
            return gson.toJson(newUser);
        });

        // Mengupdate pengguna berdasarkan ID
        put("/users/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            User user = getUserById(id);
            if (user != null) {
                User updatedUser = gson.fromJson(req.body(), User.class);
                updateUser(user, updatedUser);
                return gson.toJson(user);
            } else {
                res.status(404);
                return "User not found";
            }
        });

        // Menghapus pengguna berdasarkan ID
        delete("/users/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            User user = getUserById(id);
            if (user != null) {
                deleteUser(user);
                return "User deleted";
            } else {
                res.status(404);
                return "User not found";
            }
        });
    }

    // Mendapatkan semua pengguna dari database
    private static List<User> getAllUsers() {
        List<User> users = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM user");

            while (resultSet.next()) {
                User user = new User(
                    resultSet.getInt("id_user"),
                    resultSet.getString("nama_lengkap"),
                    resultSet.getString("username"),
                    resultSet.getString("password"),
                    resultSet.getString("email"),
                    resultSet.getString("kelamin"),
                    resultSet.getString("nomor_telepon"),
                    resultSet.getString("tanggal_lahir"),
                    resultSet.getString("kota_tinggal"),
                    resultSet.getString("kecamatan")
                );
                users.add(user);
            }

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    // Mendapatkan pengguna berdasarkan ID dari database
    private static User getUserById(int id) {
        User user = null;

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM user WHERE id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user = new User(
                    resultSet.getInt("id"),
                    resultSet.getString("namalengkap"),
                    resultSet.getString("username"),
                    resultSet.getString("password"),
                    resultSet.getString("email"),
                    resultSet.getString("kelamin"),
                    resultSet.getString("nomortelepon"),
                    resultSet.getString("tanggallahir"),
                    resultSet.getString("kotatinggal"),
                    resultSet.getString("kecamatan")
                );
            }

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    // Membuat pengguna baru dan menyimpannya ke database
    private static int createUser(User user) {
        int generatedId = -1;

        try {
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO user (namalengkap, username, password, email, kelamin, nomortelepon, tanggallahir, kotatinggal, kecamatan) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS
            );
            statement.setString(1, user.getNamaLengkap());
            statement.setString(2, user.getUsername());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getKelamin());
            statement.setString(6, user.getNomorTelepon());
            statement.setString(7, user.getTanggalLahir());
            statement.setString(8, user.getKotaTinggal());
            statement.setString(9, user.getKecamatan());

            int affectedRows = statement.executeUpdate();
            if (affectedRows > 0) {
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    generatedId = generatedKeys.getInt(1);
                }
            }

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return generatedId;
    }

    // Mengupdate pengguna di database
    private static void updateUser(User user, User updatedUser) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                "UPDATE user SET namalengkap = ?, username = ?, password = ?, email = ?, " +
                "kelamin = ?, nomortelepon = ?, tanggallahir = ?, kotatinggal = ?, kecamatan = ? WHERE id = ?"
            );
            statement.setString(1, updatedUser.getNamaLengkap());
            statement.setString(2, updatedUser.getUsername());
            statement.setString(3, updatedUser.getPassword());
            statement.setString(4, updatedUser.getEmail());
            statement.setString(5, updatedUser.getKelamin());
            statement.setString(6, updatedUser.getNomorTelepon());
            statement.setString(7, updatedUser.getTanggalLahir());
            statement.setString(8, updatedUser.getKotaTinggal());
            statement.setString(9, updatedUser.getKecamatan());
            statement.setInt(10, user.getId());

            statement.executeUpdate();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Menghapus pengguna dari database
    private static void deleteUser(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM user WHERE id = ?");
            statement.setInt(1, user.getId());

            statement.executeUpdate();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
```

Kelas UserService:

- Kelas ini bertanggung jawab untuk menangani logika bisnis terkait pengguna dalam sistem.
- Pada metode main, kelas ini menggunakan framework Spark untuk menentukan rute-endpoint yang akan diimplementasikan sebagai API.
- Terdapat beberapa rute-endpoint yang diimplementasikan di kelas ini:

    - get/users: Mengembalikan daftar semua pengguna dalam bentuk JSON.
    - get/users/:id: Mengembalikan pengguna berdasarkan ID yang diberikan dalam bentuk JSON. Jika pengguna tidak ditemukan, mengembalikan respons dengan kode status 404 dan pesan "User not found".
    - post/users: Menerima permintaan POST untuk membuat pengguna baru. Data pengguna dikirim sebagai JSON dalam tubuh permintaan. Pengguna baru akan ditambahkan ke database dan respons berisi data pengguna yang berhasil ditambahkan.
    - put/users/:id: Menerima permintaan PUT untuk memperbarui pengguna berdasarkan ID yang diberikan. Data pengguna yang diperbarui dikirim sebagai JSON dalam tubuh permintaan. Jika pengguna ditemukan, data pengguna akan diperbarui dalam database dan respons berisi data pengguna yang diperbarui. Jika pengguna tidak ditemukan, mengembalikan respons dengan kode status 404 dan pesan "User not found".
    - delete/users/:id: Menerima permintaan DELETE untuk menghapus pengguna berdasarkan ID yang diberikan. Jika pengguna ditemukan, pengguna akan dihapus dari database dan respons berisi pesan "User deleted". Jika pengguna tidak ditemukan, mengembalikan respons dengan kode status 404 dan pesan "User not found".

- Kelas ini juga memiliki metode-metode utilitas yang digunakan untuk mengakses database dan melakukan operasi seperti mendapatkan semua pengguna, mendapatkan pengguna berdasarkan ID, membuat pengguna baru, memperbarui pengguna, dan menghapus pengguna.

- Kelas API_SPARK
kelas untuk menjalankan Web Service

```java
package Akun;

public class API_SPARK {
    public static void main(String[] args) {
        UserService.main(args);
    }
}
```

- Kelas ini berfungsi sebagai poin masuk (entry point) untuk menjalankan aplikasi.
- Metode main dalam kelas ini hanya memanggil metode main dari kelas UserService. Ini dilakukan agar aplikasi dapat dijalankan dengan menggunakan Spark dan rute-endpoint yang didefinisikan di kelas UserService dapat diakses.

Untuk mengaksesnya yakni dengan menjalankan kelas ``API_SPARK``. Kemudian masukkan url nya. disini urlnya adalah ``http://localhost:4567/users``

berikut hasilnya menggunakan software ``POSTMAN`` untuk mengakses Web Service :

![WEBSERVICE](Assets/WEBSERVICE.png)

# Nomor 7
Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
---

Aplikasi yang saya buat ini menggunakan Java Swing sebagai Library GUI. Java Swing adalah sebuah toolkit pengembangan aplikasi desktop untuk bahasa pemrograman Java. Swing menyediakan berbagai komponen GUI yang dapat digunakan untuk membuat antarmuka pengguna yang interaktif dan menarik. 

Dengan menggunakan IDE Apache Netbeans library GUI Java Swing ini sudah built in didalamnya, untuk pembuatan GUI ini sangat mudah, kita hanya drag and drop komponen komponen yang dibutuhkan dari palette yang disediakan oleh netbeans.

Beberapa komponen yang digunakan ialah :
- JFrame : Digunakan sebagai jendela utama aplikasi
- JButton : Digunakan untuk membuat tombol yang dapat diklik
- JLabel : Digunakan untuk membuat tampilan teks
- JTextField: Digunakan untuk memasukkan teks atau data dari pengguna.
- JToggleButton: Digunakan untuk membuat tombol yang dapat diaktifkan dan dinonaktifkan.
- JComboBox: Digunakan untuk membuat pilihan dropdown dengan opsi yang dapat dipilih.
- JTable: Digunakan untuk menampilkan data dalam bentuk tabel.
- JDateChooser : Digunakan untuk memilih tanggal

Dan lain lain kurang lebih seperti ini.

Berikut Merupakan Gambaran GUI dari apikasi ini menggunakan Java Swing :

![GUI](Assets/GUI.gif)

# Nomor 8
Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
---

# Nomor 9
Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
---

[Youtube](https://youtu.be/9Er_GpwejXs)
# Nomor 10
Mendemonstrasikan penggunaan Machine Learning pada produk yang digunakan
---
