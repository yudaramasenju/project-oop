# Praktikum Pemrograman Berbasis Objek

# Nomor 1
[SourceCode](TravelokaApp/src/pemesanan/TiketPesawatForm.java)

# Nomor 2
[SourceCode](https://gitlab.com/yudaramasenju/project-oop/-/tree/main/TravelokaApp)

# Nomor 3
Pemrograman berbasis objek atau object oriented programming adalah paradigma Pemrograman yang berfokus pada objek dan interaksi objek untuk membangun sebuah aplikasi. Jadi paradigma ini menggunakan dasar Pemrograman objek. dimana setiap objek memiliki sifat dan perilaku tertentu yang ditentukan oleh kelas.

dalam oop,sebuah program mendefinisikan kelas kelas yang menyatakan konsep dari dunia nyata. setiap objek ini memiliki atribut dan method yang berkaitan dengan konsep tersebut.

keuntungan Pemrograman berbasis objek adalah memudahkan pemeliharaan kode, selain itu kode menjadi lebih mudah dipahami dan terorganisir, serta lebih aman dan efisien.

# Nomor 4
[SourceCode](TravelokaApp/src/pemesanan/Hotel.java)

# Nomor 5

[SourceCode](TravelokaApp/src/pemesanan/Tiket.java)

# Nomor 6

[SourceCode](TravelokaApp/src/pemesanan/TiketKeretaApi.java)

# Nomor 7

- Use case user

| NO |  Use Case |
| ------ | ----- |
| 1 | Menambahkan akun baru|
| 2 | Membaca setiap informasi aplikasi yang ada |
| 3 | Mengupdate informasi pribadi/profile |
| 4 | Menghapus akun mereka jika tidak menggunakan Traveloka lagi  |
| 5 | User Bisa Pesan Tiket Pesawat   |
| 6 | User Bisa Pesan Hotel      |
| 7 | User Bisa Pesan Tiket Kereta Api |
| 8 | User Bisa Pesan Tiket Bus |
| 9 | User Bisa Mendapatkan Promo/diskon Terhadap pemesanan tiket |
| 10 | User bisa memberikan ulasan |
| 11 | User bisa memberikan rating |
| 12 | User bisa membatalkan pesanan |
| 13 | User melakukan pencarian tiket pesawat, kereta api, dan hotel |
| 14 | User melakukan pembayaran atas pemesanan tiket yang dipilih |
| 15 | User dapat melihat ulasan informasi t destinasi wisata tertentu |
| 16 | User dapat melihat riwayat tiket yang dipesan |
| 17 | User dapat melakukan pengaturan informasi akun |
| 18 | User dapat menghubungi layanan pelanggan untuk mendapatkan bantuan atau informasi lebih lanjut |
| 19 | User dapat mengganti jadwal pemesanan tiket tertentu |
| 20 | User dapat mengakses informasi tentang fasilitas dan layanan tambahan yang tersedia di hotel atau maskapai tertentu, seperti makanan khusus atau akses ke lounge VIP |
| 21 | User dapat mengunduh atau mencetak tiket atau dokumen penting terkait perjalanan atau penginapan yang sudah dipesan |
| 22 | User dapat melakukan perbandingan antara harga atau fasilitas yang ditawarkan oleh beberapa maskapai atau hotel berbeda, sehingga dapat memilih opsi yang paling sesuai dengan kebutuhan dan anggaran |
| 23 | User dapat Mengikuti program loyalty atau member untuk mendapatkan potongan harga atau keuntungan lain ketika melakukan pemesanan di Traveloka |
| 24 | User dapat memilih tempat duduk atau kamar hotel yang diinginkan, jika tersedia opsi untuk memilih sendiri |
| 25 | User dapat memilih tujuan destinati |
| 26 | User dapat memilih tipe kamar |
| 27 | User dapat melihat rentang harga tiket dari termurah dan termahal |
| 28 | User dapat mencari tiket konser |
| 29 | User dapat memlilih kategori tiket |
| 30 | User dapat melaukan pemesanan tiket konser |

- Use case manajemen perusahaan

| NO | Use Case |
|----|----------|
| 1  | Membuat dan mengelola akun pengguna admin |
| 2  | Membaca informasi pengguna, termasuk data pribadi dan riwayat pemesanan |
| 3  | Mengupdate informasi pengguna, seperti alamat email, nomor telepon, atau preferensi |
| 4  | Menghapus akun pengguna jika diperlukan |
| 5  | Menambahkan dan mengelola informasi tentang maskapai penerbangan, hotel, atau penyedia aktivitas wisata |
| 6  | Membaca informasi tentang maskapai penerbangan, hotel, atau aktivitas wisata, termasuk harga, ketersediaan, dan ulasan pengguna |
| 7  | Mengupdate informasi tentang maskapai penerbangan, hotel, atau aktivitas wisata, seperti harga, fasilitas, atau deskripsi |
| 8  | Menghapus informasi yang sudah tidak relevan atau tidak digunakan lagi, misalnya jika maskapai penerbangan atau hotel tidak bekerja sama lagi |
| 9  | Membuat dan mengelola promosi atau diskon untuk pemesanan tiket |
| 10 | Menganalisis data penjualan, pendapatan, dan pengguna untuk mendapatkan wawasan bisnis |
| 11 | Melihat laporan dan statistik mengenai kinerja bisnis secara keseluruhan |
| 12 | Mengelola hubungan dengan mitra bisnis, termasuk maskapai penerbangan, hotel, atau penyedia aktivitas wisata |
| 13 | Melakukan integrasi dengan sistem atau platform lain yang terkait |
| 14 | Mengelola inventori hotel, termasuk ketersediaan kamar, harga, dan promosi |
| 15 | Mengelola dan menganalisis data pelanggan, termasuk riwayat pemesanan dan preferensi pengguna |
| 16 | Mengawasi dan memantau operasional perusahaan secara real-time |
| 17 | Melihat dan menganalisis data tentang kepuasan pelanggan dan masalah yang terkait |
| 18 | Mengakses informasi tentang performa produk dan layanan, serta melihat tren pasar |
| 19 | Menggunakan alat analisis untuk membuat laporan kinerja dan proyeksi bisnis |
| 20 | Mengatur dan melacak kegiatan pemasaran, termasuk kampanye promosi dan iklan |

- Use case direksi perusahaan (dashboard, monitoring, analisis)

| NO | Use Case |
|----|----------|
| 1  | Melihat dashboard yang memberikan ringkasan kinerja perusahaan secara keseluruhan, termasuk pendapatan, laba, dan metrik kunci lainnya. |
| 2  | Memantau operasional perusahaan secara real-time, termasuk jumlah pemesanan, ketersediaan stok, dan status pengiriman. |
| 3  | Menerima peringatan atau notifikasi jika ada situasi darurat atau ketidaknormalan yang membutuhkan perhatian segera. |
| 4  | Melihat grafik dan diagram visual untuk memvisualisasikan data bisnis secara efektif. |
| 5  | Menyediakan akses cepat ke informasi penting, laporan, dan dokumen terkait. |
| 6  | Melacak kinerja tim atau departemen tertentu dalam menjalankan tugas mereka. |
| 7  | Memantau dan menganalisis tingkat kepuasan pelanggan berdasarkan umpan balik dan ulasan mereka. |
| 8  | Menganalisis data penjualan, pendapatan, dan keuntungan untuk mengidentifikasi tren dan peluang bisnis. |
| 9  | Melakukan segmentasi pelanggan berdasarkan preferensi, perilaku, dan demografi untuk menginformasikan strategi pemasaran dan penargetan. |
| 10 | Menyediakan alat prediksi dan proyeksi untuk membantu dalam perencanaan bisnis dan pengambilan keputusan jangka panjang. |
| 11 | Mengidentifikasi masalah operasional atau bottlenecks yang mempengaruhi efisiensi dan kualitas layanan. |
| 12 | Menggunakan analisis data untuk mengoptimalkan harga, promosi, dan strategi penjualan. |
| 13 | Melihat laporan dan statistik mengenai kinerja bisnis secara keseluruhan. |
| 14 | Menganalisis data pelanggan untuk mengidentifikasi peluang upselling, cross-selling, dan retensi. |
| 15 | Memantau dan mengelola risiko perusahaan, termasuk risiko keuangan, operasional, dan kepatuhan. |
| 16 | Melakukan pemantauan pasar dan analisis pesaing untuk menginformasikan strategi bisnis. |
| 17 | Mengatur dan melacak kegiatan pemasaran, termasuk kampanye promosi dan iklan. |
| 18 | Menggunakan alat analisis untuk membuat laporan kinerja dan proyeksi bisnis. |
| 19 | Mengelola hubungan dengan mitra bisnis, termasuk maskapai penerbangan, hotel, atau penyedia aktivitas wisata. |
| 20 | Melakukan integrasi dengan sistem atau platform lain yang terkait. |

# Nomor 8

[ClassDiagram](TravelokaApp/Diagram/Class Diagram.png)

# Nomor 9

[Youtube](https://youtu.be/9Er_GpwejXs)

# Nomor 10
[Screenshot Aplikasi](https://gitlab.com/yudaramasenju/project-oop/-/tree/main/TravelokaApp/Assets)
