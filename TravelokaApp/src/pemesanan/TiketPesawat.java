/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pemesanan;

import java.sql.Date;

public class TiketPesawat extends Tiket {
    private String nomorPenerbangan;
    private String namaMaskapai;
    private Date tanggalKeberangkatan;
    private String kelasPenerbangan;
    private int idMaskapai;

    public TiketPesawat(int idTiket, String nomorPenerbangan, String namaMaskapai, Date tanggalKeberangkatan,
                        String asal, String tujuan, String kelasPenerbangan, int harga,
                         int idMaskapai) {
        super(idTiket, asal, tujuan, harga);
        this.nomorPenerbangan = nomorPenerbangan;
        this.namaMaskapai = namaMaskapai;
        this.tanggalKeberangkatan = tanggalKeberangkatan;
        this.kelasPenerbangan = kelasPenerbangan;
        this.idMaskapai = idMaskapai;
    }

    public String getNomorPenerbangan() {
        return nomorPenerbangan;
    }

    public String getNamaMaskapai() {
        return namaMaskapai;
    }

    public Date getTanggalKeberangkatan() {
        return tanggalKeberangkatan;
    }

    public String getKelasPenerbangan() {
        return kelasPenerbangan;
    }

    public int getIdMaskapai() {
        return idMaskapai;
    }

    @Override
    public String getTipeTiket() {
        return "Pesawat";
    }
}