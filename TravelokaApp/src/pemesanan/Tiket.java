/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pemesanan;

/**
 *
 * @author LENOVO
 */


public abstract class Tiket {
    protected int idTiket;
    protected String asal;
    protected String tujuan;
    protected int harga;

    public Tiket(int idTiket, String asal, String tujuan, int harga) {
        this.idTiket = idTiket;
        this.asal = asal;
        this.tujuan = tujuan;
        this.harga = harga;
    }

    public int getIdTiket() {
        return idTiket;
    }

    public String getAsal() {
        return asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public int getHarga() {
        return harga;
    }

    public abstract String getTipeTiket();
}