/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pemesanan;
import java.util.Date;
/**
 *
 * @author LENOVO
 */
class TiketKeretaApi extends Tiket{
   private String namaKereta;
   private Date tanggalBerangkat;
   private String kelas;
   
   public TiketKeretaApi(int idTiket, String asal, String tujuan, int harga,String namaKereta, Date tanggalBerangkat,String kelas){
       super(idTiket,asal,tujuan,harga);
       this.namaKereta = namaKereta;
       this.tanggalBerangkat = tanggalBerangkat;
       this.kelas = kelas;
   }
   
   public String getNamaKereta(){
    return namaKereta;
}
   public Date getTanggalBerangkat(){
       return tanggalBerangkat;
   }
   
   public String getKelas(){
       return kelas;
   }
   
   @Override
    public String getTipeTiket() {
        return "Kereta Api";
    }
}
