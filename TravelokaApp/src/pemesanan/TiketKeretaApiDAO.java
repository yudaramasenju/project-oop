package pemesanan;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TiketKeretaApiDAO {
    private Connection connection;

    public TiketKeretaApiDAO() {
        connection = DatabaseConnection.getInstance().getConnection();
    }

    public List<TiketKeretaApi> getAllTiketKeretaApi() {
        List<TiketKeretaApi> tiketList = new ArrayList<>();

        try {
            String query = "SELECT * FROM tiket_kereta_api";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            
            while (resultSet.next()) {
                int idTiket = resultSet.getInt("id_tiket_kereta_api");
                String namaKereta = resultSet.getString("nama_kereta");
                String asal = resultSet.getString("asal");
                String tujuan = resultSet.getString("tujuan");
                Date tanggalBerangkat = resultSet.getDate("tanggal_Berangkat");
                int harga = resultSet.getInt("harga");
                String kelas = resultSet.getString("kelas");

                TiketKeretaApi tiketkeretaapi = new TiketKeretaApi(idTiket, asal, tujuan, harga, namaKereta,
                        tanggalBerangkat, kelas);
                tiketList.add(tiketkeretaapi);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tiketList;
    }
}