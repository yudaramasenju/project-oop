package pemesanan;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TiketPesawatDAO {
    private Connection connection;

    public TiketPesawatDAO() {
        connection = DatabaseConnection.getInstance().getConnection();
    }

    public List<TiketPesawat> getAllTiketPesawat() {
        List<TiketPesawat> tiketList = new ArrayList<>();

        try {
            String query = "SELECT * FROM tiket_pesawat";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            
            while (resultSet.next()) {
                int idTiket = resultSet.getInt("id_tiket_pesawat");
                String nomorPenerbangan = resultSet.getString("nomor_penerbangan");
                String namaMaskapai = resultSet.getString("nama_maskapai");
                Date tanggalKeberangkatan = resultSet.getDate("tanggal_keberangkatan");
                String asal = resultSet.getString("asal");
                String tujuan = resultSet.getString("tujuan");
                String kelasPenerbangan = resultSet.getString("kelas_penerbangan");
                int harga = resultSet.getInt("harga");
                int idMaskapai = resultSet.getInt("id_maskapai");
                
                TiketPesawat tiketPesawat = new TiketPesawat(idTiket, nomorPenerbangan, namaMaskapai,
                        tanggalKeberangkatan, asal, tujuan, kelasPenerbangan, harga,idMaskapai);
                tiketList.add(tiketPesawat);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tiketList;
    }
}