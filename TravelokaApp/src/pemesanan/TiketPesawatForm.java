/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package pemesanan;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author LENOVO
 */
public class TiketPesawatForm extends javax.swing.JFrame {
    private TiketPesawatDAO tiketPesawatDAO;
    private String maskapai,tujuan,asal,namaPenumpang,email;
    private int totalHarga;
    
    public TiketPesawatForm() {
        tiketPesawatDAO = new TiketPesawatDAO();
        maskapai = "Semua"; // Inisialisasi dengan nilai default
        tujuan = "Semua"; // Inisialisasi dengan nilai default
        asal = "Semua"; // Inisialisasi dengan nilai default
        setTitle("Tiket Pesawat");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents();
        setLocationRelativeTo(this);
        showTiketPesawat();
        
        // Tambahkan pendengar acara ke ComboBox cbJumlahTiket
        cbJumlahTiket.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
            updateTotalHarga();
        }
    });
  }
    
    private void showTiketPesawat() {
    DefaultTableModel tableModel = (DefaultTableModel) jTable1.getModel();

    // Hapus semua baris yang ada di dalam model
    tableModel.setRowCount(0);

    // Mengambil data tiket pesawat dari database menggunakan TiketPesawatDAO
    List<TiketPesawat> tiketList = tiketPesawatDAO.getAllTiketPesawat();
    
    // Filter tiket berdasarkan tujuan
    List<TiketPesawat> filteredTiketList = new ArrayList<>();
    for (TiketPesawat tiket : tiketList) {
    if (maskapai.equals("Semua") && tujuan.equals("Semua") && asal.equals("Semua")) {
        // Jika maskapai, tujuan, dan asal adalah "Semua", tambahkan semua tiket
        filteredTiketList.add(tiket);
    } else if (maskapai.equals("Semua") && tujuan.equals("Semua")) {
        // Jika maskapai dan tujuan adalah "Semua", filter berdasarkan asal saja
        if (tiket.getAsal().equals(asal)) {
            filteredTiketList.add(tiket);
        }
    } else if (maskapai.equals("Semua") && asal.equals("Semua")) {
        // Jika maskapai dan asal adalah "Semua", filter berdasarkan tujuan saja
        if (tiket.getTujuan().equals(tujuan)) {
            filteredTiketList.add(tiket);
        }
    } else if (tujuan.equals("Semua") && asal.equals("Semua")) {
        // Jika tujuan dan asal adalah "Semua", filter berdasarkan maskapai saja
        if (tiket.getNamaMaskapai().equals(maskapai)) {
            filteredTiketList.add(tiket);
        }
    } else if (maskapai.equals("Semua")) {
        // Jika maskapai adalah "Semua", filter berdasarkan tujuan dan asal
        if (tiket.getTujuan().equals(tujuan) && tiket.getAsal().equals(asal)) {
            filteredTiketList.add(tiket);
        }
    } else if (tujuan.equals("Semua")) {
        // Jika tujuan adalah "Semua", filter berdasarkan maskapai dan asal
        if (tiket.getNamaMaskapai().equals(maskapai) && tiket.getAsal().equals(asal)) {
            filteredTiketList.add(tiket);
        }
    } else if (asal.equals("Semua")) {
        // Jika asal adalah "Semua", filter berdasarkan maskapai dan tujuan
        if (tiket.getNamaMaskapai().equals(maskapai) && tiket.getTujuan().equals(tujuan)) {
            filteredTiketList.add(tiket);
        }
    } else {
        // Jika maskapai, tujuan, dan asal bukan "Semua", filter berdasarkan maskapai, tujuan, dan asal
        if (tiket.getNamaMaskapai().equals(maskapai) && tiket.getTujuan().equals(tujuan) && tiket.getAsal().equals(asal)) {
            filteredTiketList.add(tiket);
        }
    }
}
    // Tambahkan baris-baris data tiket pesawat ke dalam model
    for (TiketPesawat tiket : filteredTiketList) {
        Object[] rowData = {
            tiket.getNomorPenerbangan(),
            tiket.getNamaMaskapai(),
            tiket.getTanggalKeberangkatan(),
            tiket.getAsal(),
            tiket.getTujuan(),
            tiket.getKelasPenerbangan(),
            tiket.getHarga()
        };
        tableModel.addRow(rowData);
    }

}
    private void updateTotalHarga() {
        int harga = Integer.parseInt(txtHarga.getText());
        int jumlahTiket = Integer.parseInt(cbJumlahTiket.getSelectedItem().toString());
        int totalHarga = harga * jumlahTiket;
        txtTotalHarga.setText(Integer.toString(totalHarga));
}

    
    private void pesanTiket() {
        // Mendapatkan data dari inputan pengguna
        String namaPengguna = txtNamaPenumpang.getText();
        email = txtEmail.getText();
        int selectedRow = jTable1.getSelectedRow();
        asal = jTable1.getValueAt(selectedRow, 3).toString();
        tujuan = jTable1.getValueAt(selectedRow, 4).toString();
        maskapai = jTable1.getValueAt(selectedRow, 1).toString();
        int harga = Integer.parseInt(txtHarga.getText());
        int jumlahTiket = Integer.parseInt(cbJumlahTiket.getSelectedItem().toString());
        
        // Hitung total harga
        totalHarga = harga * jumlahTiket;
        
        txtTotalHarga.setText(Integer.toString(totalHarga));

        // Panggil updateTotalHarga()
        updateTotalHarga();
        
        
        // Mendapatkan jumlah uang yang dibayarkan
        int uangBayar = Integer.parseInt(txtBayar.getText());

        // Hitung kembalian
        int kembalian = uangBayar - totalHarga;
        
        // Menampilkan informasi tiket
        String informasiTiket = "Informasi Tiket:\n\n" +
                "Nama Penumpang: " + namaPengguna + "\n" +
                "Email: " + email + "\n" +
                "Asal: " + asal + "\n" +
                "Tujuan: " + tujuan + "\n" +
                "Maskapai: " + maskapai + "\n" +
                "Harga: " + harga + "\n" +
                "Jumlah Tiket: " + jumlahTiket + "\n" +
                "Total Harga: " + totalHarga + "\n"+
                "Kembalian: " + kembalian + "\n";

        // Menampilkan dialog informasi tiket
        JOptionPane.showMessageDialog(this, informasiTiket, "Informasi Tiket", JOptionPane.INFORMATION_MESSAGE);
        
        try {
            // Tentukan path atau lokasi file txt untuk menyimpan informasi tiket
            String path = "C:\\Users\\LENOVO\\Documents\\NetBeansProjects\\TravelokaApp/CetakTiket.txt";

            // Buat objek FileWriter dan BufferedWriter
            FileWriter fileWriter = new FileWriter(path);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            // Tulis informasi tiket ke dalam file txt
            bufferedWriter.write(informasiTiket);

            // Tutup BufferedWriter
            bufferedWriter.close();

            // Tampilkan notifikasi bahwa informasi tiket telah berhasil dicetak ke file txt
            JOptionPane.showMessageDialog(this, "Informasi tiket berhasil dicetak");

            // Kembali ke menu
            MainMenu menu = new MainMenu();
            menu.setVisible(true);
            this.dispose();
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Terjadi kesalahan saat mencetak informasi tiket!");
        }
}
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jScrollBar1 = new javax.swing.JScrollBar();
        jPanel1 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtNamaPenumpang = new javax.swing.JTextField();
        btnPesan = new javax.swing.JToggleButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        cbMaskapai = new javax.swing.JComboBox<>();
        cbTujuan = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        cbAsal = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtHarga = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        cbJumlahTiket = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        txtTotalHarga = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtBayar = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(27, 160, 226));
        jPanel1.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jPanel1.setPreferredSize(new java.awt.Dimension(632, 442));

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel2.setText("Nama :");

        txtNamaPenumpang.setDragEnabled(true);
        txtNamaPenumpang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNamaPenumpangActionPerformed(evt);
            }
        });

        btnPesan.setText("Pesan");
        btnPesan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesanActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        jLabel4.setText("Tujuan :");

        jLabel6.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        jLabel6.setText("Asal :");

        jLabel3.setText("Email :");

        txtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        jLabel7.setText("Maskapai :");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Nomor Penerbangan", "Nama Maskapai", "Tanggal", "Asal", "Tujuan", "Kelas", "Harga"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable1);

        cbMaskapai.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        cbMaskapai.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Semua", "Air Asia", "Batik Air", "Citilink", "Garuda Indonesia", "Lion Air", "SuperAirJet", "Wings Air" }));
        cbMaskapai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaskapaiActionPerformed(evt);
            }
        });

        cbTujuan.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        cbTujuan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Semua", "Surabaya", "Yogyakarta" }));
        cbTujuan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTujuanActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel8.setText("DAFTAR TIKET");

        cbAsal.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        cbAsal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Semua", "Bandung", "Jakarta" }));
        cbAsal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbAsalActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel9.setText("ISI INFORMASI");

        jLabel5.setText("Harga :");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("sansserif", 1, 24)); // NOI18N
        jLabel1.setText("TIKET PESAWAT");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtHarga.setEditable(false);
        txtHarga.setBackground(new java.awt.Color(204, 204, 255));
        txtHarga.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        txtHarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHargaActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        jLabel10.setText("Jumlah Tiket :");

        cbJumlahTiket.setFont(new java.awt.Font("SansSerif", 0, 12)); // NOI18N
        cbJumlahTiket.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1", "2", "3", "4", "5" }));
        cbJumlahTiket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbJumlahTiketActionPerformed(evt);
            }
        });

        jLabel11.setText("Total Harga :");

        txtTotalHarga.setEditable(false);
        txtTotalHarga.setBackground(new java.awt.Color(204, 204, 255));
        txtTotalHarga.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        txtTotalHarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalHargaActionPerformed(evt);
            }
        });

        jLabel12.setText("Bayar :");

        txtBayar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBayarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPesan, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(jLabel9))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbJumlahTiket, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel2))
                                        .addGap(31, 31, 31))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtEmail)
                                    .addComponent(cbAsal, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbTujuan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbMaskapai, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtHarga)
                                    .addComponent(txtNamaPenumpang, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(248, 248, 248)
                        .addComponent(jLabel8)
                        .addContainerGap(263, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel12))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtBayar)
                                    .addComponent(txtTotalHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 595, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNamaPenumpang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbAsal, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbTujuan, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbMaskapai, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(txtTotalHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txtHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBayar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbJumlahTiket, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPesan)
                    .addComponent(btnBack))
                .addGap(40, 40, 40))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 445, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        MainMenu menu = new MainMenu();
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void txtNamaPenumpangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNamaPenumpangActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNamaPenumpangActionPerformed

    private void btnPesanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesanActionPerformed
        if (txtNamaPenumpang.getText().isEmpty() && txtEmail.getText().isEmpty() && cbAsal.getSelectedIndex() == -1 && cbTujuan.getSelectedIndex() == -1) {
        JOptionPane.showMessageDialog(this, "Silakan isi semua informasi terlebih dahulu!");
        return;
    }
    
        // Validasi inputan pengguna (apakah jumlah uang bayar sudah diisi)
        if (txtBayar.getText().isEmpty()) {
        JOptionPane.showMessageDialog(this, "Mohon masukkan jumlah uang bayar!");
        return;
    }
        pesanTiket();
    }//GEN-LAST:event_btnPesanActionPerformed

    private void txtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
            // Ambil indeks baris yang dipilih
            int selectedRow = jTable1.getSelectedRow();

            // Ambil data dari baris yang dipilih
            String nomorPenerbangan = jTable1.getValueAt(selectedRow, 0).toString();
            String namaMaskapai = jTable1.getValueAt(selectedRow, 1).toString();
            String tanggalKeberangkatan = jTable1.getValueAt(selectedRow, 2).toString();
            String asal = jTable1.getValueAt(selectedRow, 3).toString();
            String tujuan = jTable1.getValueAt(selectedRow, 4).toString();
            String kelasPenerbangan = jTable1.getValueAt(selectedRow, 5).toString();
            String harga = jTable1.getValueAt(selectedRow, 6).toString();

            // Set nilai yang diambil ke field-form pemesanan
            txtHarga.setText(harga);
            updateTotalHarga();
    }//GEN-LAST:event_jTable1MouseClicked

    private void cbMaskapaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMaskapaiActionPerformed
        maskapai = cbMaskapai.getSelectedItem().toString();
        showTiketPesawat();
    }//GEN-LAST:event_cbMaskapaiActionPerformed

    private void cbTujuanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTujuanActionPerformed
        tujuan = cbTujuan.getSelectedItem().toString();
        showTiketPesawat();
    }//GEN-LAST:event_cbTujuanActionPerformed

    private void cbAsalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAsalActionPerformed
        asal = cbAsal.getSelectedItem().toString();
        showTiketPesawat();
    }//GEN-LAST:event_cbAsalActionPerformed

    private void txtHargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHargaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHargaActionPerformed

    private void cbJumlahTiketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbJumlahTiketActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbJumlahTiketActionPerformed

    private void txtTotalHargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalHargaActionPerformed
        pesanTiket();
    }//GEN-LAST:event_txtTotalHargaActionPerformed

    private void txtBayarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBayarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBayarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TiketPesawatForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TiketPesawatForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TiketPesawatForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TiketPesawatForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TiketPesawatForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JToggleButton btnPesan;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox<String> cbAsal;
    private javax.swing.JComboBox<String> cbJumlahTiket;
    private javax.swing.JComboBox<String> cbMaskapai;
    private javax.swing.JComboBox<String> cbTujuan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBayar;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtHarga;
    private javax.swing.JTextField txtNamaPenumpang;
    private javax.swing.JTextField txtTotalHarga;
    // End of variables declaration//GEN-END:variables

}
