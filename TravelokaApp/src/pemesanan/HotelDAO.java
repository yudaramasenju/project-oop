package pemesanan;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HotelDAO {
    private Connection connection;

    public HotelDAO() {
        connection = DatabaseConnection.getInstance().getConnection();
    }

    public List<Hotel> getAllHotel() {
        List<Hotel> tiketList = new ArrayList<>();

        try {
            String query = "SELECT * FROM hotel";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            
            while (resultSet.next()) {
                int idTiket = resultSet.getInt("id_hotel");
                String namaHotel = resultSet.getString("nama_hotel");
                String kota = resultSet.getString("kota");
                String fasilitas = resultSet.getString("fasilitas");
                int harga = resultSet.getInt("harga");
                int rating = resultSet.getInt("rating");
                
                Hotel hotel = new Hotel(idTiket, namaHotel, kota, fasilitas, harga, rating);
                tiketList.add(hotel);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tiketList;
    }
}