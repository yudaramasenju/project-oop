package pemesanan;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TravelokaAppGUI extends JFrame {

    private static final String URL_STRING = "https://api.traveloka.com/flights";

    private JLabel responseLabel;

    public TravelokaAppGUI() {
        setTitle("Traveloka App");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);
        setLocationRelativeTo(null);

        responseLabel = new JLabel();

        JButton requestButton = new JButton("Send HTTP Request");
        requestButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String response = sendHttpRequest(URL_STRING);
                    responseLabel.setText(response);
                } catch (IOException ex) {
                    responseLabel.setText("Error: " + ex.getMessage());
                }
            }
        });

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(requestButton, BorderLayout.NORTH);
        mainPanel.add(responseLabel, BorderLayout.CENTER);

        add(mainPanel);
    }

    private String sendHttpRequest(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            return response.toString();
        } else {
            throw new IOException("HTTP request failed with response code: " + responseCode);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                TravelokaAppGUI app = new TravelokaAppGUI();
                app.setVisible(true);
            }
        });
    }
}