package pemesanan;

/**
 *
 * @author LENOVO
 */
public class Hotel {
    private int idHotel;
    private String namaHotel;
    private String kota;
    private String fasilitas;
    private int harga;
    private int rating;
    
    public Hotel(int idHotel, String namaHotel, String kota, String fasilitas, int harga, int rating){
        this.idHotel = idHotel;
        this.namaHotel = namaHotel;
        this.kota = kota;
        this.fasilitas = fasilitas;
        this.harga = harga;
        this.rating = rating;
    }
    
    // Setter
    public void setIdHotel(int idHotel) {
    this.idHotel = idHotel;
    }

    public void setNamaHotel(String namaHotel) {
        this.namaHotel = namaHotel;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setFasilitas(String fasilitas) {
        this.fasilitas = fasilitas;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
 
    // Getter
    public int getIdHotel(){
        return idHotel;
    }
    
    public String getNamaHotel(){
        return namaHotel;
    }
    
    public String getKota(){
        return kota;
    }
    
    public String getFasilitas(){
        return fasilitas;
    }
    
    public int getHarga(){
        return harga;
    }
    
    public int getRating(){
        return rating;
    }
}