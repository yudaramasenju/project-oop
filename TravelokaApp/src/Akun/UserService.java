package Akun;

import pemesanan.DatabaseConnection;
import static spark.Spark.*;
import com.google.gson.Gson;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserService {

    private static Gson gson = new Gson();
    private static Connection connection;

    public static void main(String[] args) {
        // Mendapatkan objek koneksi dari DatabaseConnection
        connection = DatabaseConnection.getInstance().getConnection();

        // Mendapatkan semua pengguna
        get("/users", (req, res) -> {
            List<User> users = getAllUsers();
            return gson.toJson(users);
        });

        // Mendapatkan pengguna berdasarkan ID
        get("/users/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            User user = getUserById(id);
            if (user != null) {
                return gson.toJson(user);
            } else {
                res.status(404);
                return "User not found";
            }
        });

        // Membuat pengguna baru
        post("/users", (req, res) -> {
            User newUser = gson.fromJson(req.body(), User.class);
            int id = createUser(newUser);
            newUser.setId(id);
            return gson.toJson(newUser);
        });

        // Mengupdate pengguna berdasarkan ID
        put("/users/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            User user = getUserById(id);
            if (user != null) {
                User updatedUser = gson.fromJson(req.body(), User.class);
                updateUser(user, updatedUser);
                return gson.toJson(user);
            } else {
                res.status(404);
                return "User not found";
            }
        });

        // Menghapus pengguna berdasarkan ID
        delete("/users/:id", (req, res) -> {
            int id = Integer.parseInt(req.params("id"));
            User user = getUserById(id);
            if (user != null) {
                deleteUser(user);
                return "User deleted";
            } else {
                res.status(404);
                return "User not found";
            }
        });
    }

    // Mendapatkan semua pengguna dari database
    private static List<User> getAllUsers() {
        List<User> users = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM user");

            while (resultSet.next()) {
                User user = new User(
                    resultSet.getInt("id_user"),
                    resultSet.getString("nama_lengkap"),
                    resultSet.getString("username"),
                    resultSet.getString("password"),
                    resultSet.getString("email"),
                    resultSet.getString("kelamin"),
                    resultSet.getString("nomor_telepon"),
                    resultSet.getString("tanggal_lahir"),
                    resultSet.getString("kota_tinggal"),
                    resultSet.getString("kecamatan")
                );
                users.add(user);
            }

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    // Mendapatkan pengguna berdasarkan ID dari database
    private static User getUserById(int id) {
        User user = null;

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM user WHERE id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user = new User(
                    resultSet.getInt("id"),
                    resultSet.getString("namalengkap"),
                    resultSet.getString("username"),
                    resultSet.getString("password"),
                    resultSet.getString("email"),
                    resultSet.getString("kelamin"),
                    resultSet.getString("nomortelepon"),
                    resultSet.getString("tanggallahir"),
                    resultSet.getString("kotatinggal"),
                    resultSet.getString("kecamatan")
                );
            }

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    // Membuat pengguna baru dan menyimpannya ke database
    private static int createUser(User user) {
        int generatedId = -1;

        try {
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO user (namalengkap, username, password, email, kelamin, nomortelepon, tanggallahir, kotatinggal, kecamatan) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS
            );
            statement.setString(1, user.getNamaLengkap());
            statement.setString(2, user.getUsername());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getKelamin());
            statement.setString(6, user.getNomorTelepon());
            statement.setString(7, user.getTanggalLahir());
            statement.setString(8, user.getKotaTinggal());
            statement.setString(9, user.getKecamatan());

            int affectedRows = statement.executeUpdate();
            if (affectedRows > 0) {
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    generatedId = generatedKeys.getInt(1);
                }
            }

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return generatedId;
    }

    // Mengupdate pengguna di database
    private static void updateUser(User user, User updatedUser) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                "UPDATE user SET namalengkap = ?, username = ?, password = ?, email = ?, " +
                "kelamin = ?, nomortelepon = ?, tanggallahir = ?, kotatinggal = ?, kecamatan = ? WHERE id = ?"
            );
            statement.setString(1, updatedUser.getNamaLengkap());
            statement.setString(2, updatedUser.getUsername());
            statement.setString(3, updatedUser.getPassword());
            statement.setString(4, updatedUser.getEmail());
            statement.setString(5, updatedUser.getKelamin());
            statement.setString(6, updatedUser.getNomorTelepon());
            statement.setString(7, updatedUser.getTanggalLahir());
            statement.setString(8, updatedUser.getKotaTinggal());
            statement.setString(9, updatedUser.getKecamatan());
            statement.setInt(10, user.getId());

            statement.executeUpdate();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Menghapus pengguna dari database
    private static void deleteUser(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM user WHERE id = ?");
            statement.setInt(1, user.getId());

            statement.executeUpdate();

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}