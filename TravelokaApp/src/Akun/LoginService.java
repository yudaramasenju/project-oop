/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Akun;

import java.sql.*;
import pemesanan.DatabaseConnection;
/**
 *
 * @author LENOVO
 */
public class LoginService {
    private DatabaseConnection databaseconnection;
    
    public LoginService() {
        // Inisialisasi koneksi ke database
        databaseconnection = DatabaseConnection.getInstance();
    }
    
    public boolean login(String username, String password) {
        boolean isAuthenticated = false;
        Connection connection = DatabaseConnection.getInstance().getConnection();
        try {
            // Mengeksekusi query untuk mencari pengguna dengan username dan password yang sesuai
            String query = "SELECT * FROM user WHERE username = ? AND password = ?";
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, username);
            st.setString(2, password);

            ResultSet rs = st.executeQuery();

            // Memeriksan query
            if (rs.next()) {
                isAuthenticated = true;
            }

            // Menutup sumber daya terkait
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isAuthenticated;
    }
}
