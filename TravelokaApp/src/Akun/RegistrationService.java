package Akun;

import pemesanan.DatabaseConnection;
import Akun.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegistrationService {
    private DatabaseConnection databaseconnection;
    
    public RegistrationService() {
        // Inisialisasi koneksi ke database
        databaseconnection = DatabaseConnection.getInstance();
    }

    public boolean register(User user) {
        boolean isRegistered = false;
        Connection connection = DatabaseConnection.getInstance().getConnection();
        try {
            // Mengeksekusi query untuk memasukkan data pengguna baru ke database
            String query = "INSERT INTO user (nama_lengkap, username, password, email, kelamin, nomor_telepon, tanggal_lahir, kota_tinggal, kecamatan) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, user.getNamaLengkap());
            statement.setString(2, user.getUsername());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getKelamin());
            statement.setString(6, user.getNomorTelepon());
            statement.setString(7, user.getTanggalLahir());
            statement.setString(8, user.getKotaTinggal());
            statement.setString(9, user.getKecamatan());

            int rowsAffected = statement.executeUpdate();

            // Memeriksa apakah data berhasil disimpan
            if (rowsAffected > 0) {
                isRegistered = true;
            }

            // Menutup sumber daya terkait
            statement.close();
 
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return isRegistered;
    }
}