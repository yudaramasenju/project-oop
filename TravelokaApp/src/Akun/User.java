    /*
     * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
     * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
     */
    package Akun;
    import java.sql.*;

    /**
     *
     * @author LENOVO
     */
    public class User {
        private int id;
        private String namalengkap, email,  kelamin, nomortelepon, kotatinggal, kecamatan;
        private String username;
        private String password;
        private String tanggallahir;

        private Connection connection;

        public User(String namalengkap, String username, String password, String email,  String kelamin, String nomortelepon, String tanggallahir, String kotatinggal, String kecamatan){
            //this.id = id;
            this.namalengkap = namalengkap;
            this.username = username;
            this.password = password;
            this.email = email;
            this.kelamin = kelamin;
            this.nomortelepon = nomortelepon;
            this.tanggallahir = tanggallahir;
            this.kotatinggal = kotatinggal;
            this.kecamatan = kecamatan;
        }
        
        public User(int id,String namalengkap, String username, String password, String email,  String kelamin, String nomortelepon, String tanggallahir, String kotatinggal, String kecamatan){
            this.id = id;
            this.namalengkap = namalengkap;
            this.username = username;
            this.password = password;
            this.email = email;
            this.kelamin = kelamin;
            this.nomortelepon = nomortelepon;
            this.tanggallahir = tanggallahir;
            this.kotatinggal = kotatinggal;
            this.kecamatan = kecamatan;
        }
        
        public void setId(int id) {
        this.id = id;
        }

        public void setNamaLengkap(String namalengkap) {
            this.namalengkap = namalengkap;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setKelamin(String kelamin) {
            this.kelamin = kelamin;
        }

        public void setNomorTelepon(String nomortelepon) {
            this.nomortelepon = nomortelepon;
        }

        public void setTanggalLahir(String tanggallahir) {
            this.tanggallahir = tanggallahir;
        }

        public void setKotaTinggal(String kotatinggal) {
            this.kotatinggal = kotatinggal;
        }

        public void setKecamatan(String kecamatan) {
            this.kecamatan = kecamatan;
        }
        
        public int getId(){
            return id;
        }
        
        public String getNamaLengkap() {
            return namalengkap;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

        public String getEmail() {
            return email;
        }

        public String getKelamin() {
            return kelamin;
        }

        public String getNomorTelepon() {
            return nomortelepon;
        }

        public String getTanggalLahir() {
            return tanggallahir;
        }

        public String getKotaTinggal() {
            return kotatinggal;
        }

        public String getKecamatan() {
            return kecamatan;
        }
    }
